
public class Main {
	public static void main(String[] args) {
		Hexagon n = new Hexagon();
		long startTime = System.nanoTime();
		n.Ejecutar();
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Total execution time in miliseconds: " + elapsedTime/1000000);
	}
}
