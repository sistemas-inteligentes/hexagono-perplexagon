import java.util.Scanner;

public class Hexagon {

//
final public static int NUMERO_DE_PIEZAS = 7;
final public static int NUMERO_DE_LADOS = 6;

public static void Ejecutar() {

	Scanner stdin = new Scanner(System.in);
	int numCases = LeerNumeroDeCasos(stdin);
	// Go through all the cases.
	for (int i=1; i<=numCases; i++) {
		// Read in original pieces in order.
		int[][] puzzle = LeerNumeros(stdin);
		// Solve.
		int[] solucion = Resolver(puzzle);
		// Output result.
		MostrarSolucion(i, solucion);
	}
}

private static void MostrarSolucion(int loop, int[] ans) {
	if (ans == null)
		System.out.println("Case "+loop+": No solution");
	else 
	{
		System.out.print("Case "+loop+":");
		for (int i=0; i<NUMERO_DE_PIEZAS; i++)
			System.out.print(" "+ans[i]);
		System.out.println();
	}
}

private static int LeerNumeroDeCasos(Scanner stdin) {
	int NumeroDeCasos = stdin.nextInt();
	return NumeroDeCasos;
}

private static int[][] LeerNumeros(Scanner stdin) {
	int[][] puzzle = new int[NUMERO_DE_PIEZAS][NUMERO_DE_LADOS];
	for (int i=0; i<NUMERO_DE_PIEZAS; i++)
		for (int j=0; j<NUMERO_DE_LADOS; j++)
			puzzle[i][j] = stdin.nextInt();
	return puzzle;
}

// Wrapper function.
private static int[] Resolver(int[][] puzzle) {
	int[] camino = new int[NUMERO_DE_PIEZAS];
	boolean[] visitados = new boolean[NUMERO_DE_PIEZAS];
	return Estrategia_De_Control_DFS(puzzle, camino, visitados, 0);
}

private static int[] Estrategia_De_Control_DFS(int[][] puzzle, int[] camino, boolean[] E_Expl, int numero_Hexagonos) {

	// We've finished a position, return our answer.
	if (Cond_term(numero_Hexagonos)) {
		if (Evaluar(puzzle, camino))
		{
			return camino;
		}
		else
			return null;
	}

	// Try each unused piece next.
	for (int i=0; i<NUMERO_DE_PIEZAS; i++) {
		if (NoVisitados(E_Expl, i)) {
			E_Expl[i] = true;
			camino[numero_Hexagonos] = i;

			// If this branch leads to a solution, stop and return!!!
			int[] auxiliar = Estrategia_De_Control_DFS(puzzle, camino, E_Expl, numero_Hexagonos+1);
			if (auxiliar != null) return auxiliar;

			// Move on...

			E_Expl[i] = false;
		}
	}

	// If we get here, nothing in this branch worked.
	return null;
}

private static boolean NoVisitados(boolean[] visitados, int i) {
	return !visitados[i];
}

private static boolean Cond_term(int numero_Hexagonos) {
	return numero_Hexagonos == NUMERO_DE_PIEZAS;
}

private static boolean Evaluar(int[][] puzzle, int[] camino) {

	// Easier if we copy over these references, for our sanity =)
	int[][] cur = new int[NUMERO_DE_PIEZAS][];
	for (int i=0; i<NUMERO_DE_PIEZAS; i++)
		cur[i] = puzzle[camino[i]];

	// Fix middle piece.
	Rotar(cur[0], 0, 1);

	// Now, fix the rest of the pieces, according to the middle piece only.
	for (int i=1; i<NUMERO_DE_PIEZAS; i++)
		Rotar(cur[i], (i+2)%NUMERO_DE_LADOS, cur[0][i-1]);


	for (int i=1; i<NUMERO_DE_PIEZAS; i++) {

		// Set piece to compare piece i.
		int siguiente = i+1;
		if (siguiente >= NUMERO_DE_PIEZAS) siguiente = 1;

		// Check sides that are supposed to match...
		if (cur[i][(i+1)%NUMERO_DE_LADOS] != cur[siguiente][(i+4)%NUMERO_DE_LADOS])
			return false;
	}

	return true;
}

// Rotates piece so that number fits in the location posMatch. (0 is top, 1 is top right, etc.)
private static void Rotar(int[] pieza, int Posicion, int number) {

	// Would normally be dangerous, but this should be fine given what we are testing.
	while (pieza[Posicion] != number)
		Girar(pieza);
}

// Shifts this piece counter clockwise.
private static void Girar(int[] pieza) {
	int auxiliar = pieza[0];
	for (int i=1; i<NUMERO_DE_LADOS; i++)
		pieza[i-1] = pieza[i];
	pieza[NUMERO_DE_LADOS-1] = auxiliar;
}
}

